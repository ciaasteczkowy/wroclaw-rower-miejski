export default class Station {
  constructor(id, system, location, latitude, longitude) {
    this.id = id
    this.system = system
    this.location = location
    this.latitude = latitude
    this.longitude = longitude
  }
}
