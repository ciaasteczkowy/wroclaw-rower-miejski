module.exports = {
  head: {
    title: 'Stacje Wrocławskiego Roweru Miejskiego',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Stacje Wrocławskiego Roweru Miejskiego' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '//cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css' }
    ]
  },
  loading: { color: '#3B8070' },
  css: [
    '~/assets/css/main.scss'
  ],
  modules: [
    '@nuxtjs/axios',
  ],
  plugins: [
    '~/plugins/buefy.js',
    '~/plugins/google-maps.js'
  ],
  build: {
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    dir: 'public/'
  },
  router: {
    base: '/wroclaw-rower-miejski/'
  },  
  axios: {}
}
