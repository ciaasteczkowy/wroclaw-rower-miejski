import Station from '~/models/Station'

export const state = () => ({
  stations: []
})

export const actions = {
  async fetch ({ commit }) {
    const stations = await this.$axios.$get('https://jsonpdataproxy.appspot.com/', {
      params: {
        format: 'json',
        url: 'http://www.wroclaw.pl/open-data/dataset/78923d5c-dd8e-423c-b873-8a8929df5b95/resource/4cce4f86-e414-4aab-a3cf-d6333ee0cea0/download/transport_stacje_wrm.xls',
        type: 'XLS'
      }
    })
    commit('SET_STATIONS', stations.data.map(row => new Station(...row)))
  }
}

export const mutations = {
  SET_STATIONS: (state, stations) => {
    state.stations = stations
      .sort((a, b) => a.location.localeCompare(b.location))
  }
}
